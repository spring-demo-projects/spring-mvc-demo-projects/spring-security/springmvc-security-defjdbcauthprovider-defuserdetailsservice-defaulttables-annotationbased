package com.cdac.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	/*
	 * Configure Authentication Manager
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		/*
		 * By default spring comes with 2 Authentication providers :
		 * 
		 * 1. JDBC Authentication Provider
		 * 
		 * 2. In-Memory Authentication Provider
		 */

		/*
		 * JDBC Authentication Provider
		 */
		auth.authenticationProvider(getJDBCAuthenticationProvider());

	}

	/*
	 * JDBC Authentication Provider
	 */
	private AuthenticationProvider getJDBCAuthenticationProvider() {

		/*
		 * DaoAuthenticationProvider :
		 * 
		 * An AuthenticationProvider implementation that retrieves user details from a
		 * UserDetailsService.
		 */
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(getJDBCUserDetailsService());
		authProvider.setPasswordEncoder(getNoPasswordEncoder());

		return authProvider;
	}

	private UserDetailsService getJDBCUserDetailsService() {
		/*
		 * JdbcDaoImpl :
		 * 
		 * UserDetailsServiceRetrieves implementation which retrieves the user details
		 * (username, password, enabled flag, and authorities) from a database using
		 * JDBC queries.
		 * 
		 * Default Schema :
		 * 
		 * A default database schema is assumed, with two tables "users" and
		 * "authorities".
		 * 
		 * The Users Table Columns : username, password, enabled
		 * 
		 * The Authorities Table Column : username, authority
		 *
		 * 
		 * If we are using an existing schema you will have to set the queries
		 * usersByUsernameQuery and authoritiesByUsernameQuery to match your database
		 * setup
		 */
		JdbcDaoImpl jdbcUserDetailsService = new JdbcDaoImpl();

		jdbcUserDetailsService.setDataSource(dataSource);

		/*
		 * We are using default table schema specified by spring for authentication.
		 * 
		 * Default queries are given below
		 * 
		 * usersByUsernameQuery :
		 * "select username,password,enabled from users where username = ?";
		 * 
		 * authoritiesByUsernameQuery :
		 * "select username,authority from authorities where username = ?";
		 */

		return jdbcUserDetailsService;
	}

	private PasswordEncoder getNoPasswordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

}
